# aasms

This is a basic module to interface with the Andrews & Arnold SMS
service at <http://www.aaisp.net.uk/kb-telecoms-sms.html>.

You obviously need to buy an account with them to make it work.

It's under the GPL.

Now supports simple config file with format:

```txt
[sms]
username=your_username
password=your_password
```

There's a public git repo available at:

<https://gitlab.com/aasms/aasms>

Author: Michael Stevens <mstevens@mstevens.org>
